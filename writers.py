import sys

from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.styles import Font, Alignment, NamedStyle, PatternFill
import numpy as np

from abc import ABC, abstractmethod


class AbstractWriter(ABC):
    @staticmethod
    @abstractmethod
    def write(data, criterion, alpha, destination):
        pass


class StreamWriter(AbstractWriter):
    @staticmethod
    def write(data, criterion, alpha, destination=sys.stdout):
        if alpha is not None:
            print(f"Criterion: {criterion}, alpha={alpha}")
        np.set_printoptions(precision=3, suppress=False)
        print(f"F(X) = {round(data['F'], 3)}")
        print("x_T:")
        print(data['x'])
        print("\ny_T:")
        print(data['y'], '\n')


class XlsxWriter(AbstractWriter):
    def write(self, data, criterion, alpha, destination):
        try:
            wbook = load_workbook(destination)
        except FileNotFoundError:
            wbook = Workbook()
        index_style = NamedStyle("index_style", font=Font(bold=True),
                                  fill=PatternFill(start_color="D3D3D3", fill_type="solid"))
        header_style = NamedStyle("header_style", font=Font(bold=True), alignment=Alignment(horizontal="center"))
        if "header_style" not in wbook.style_names:
            wbook.add_named_style(header_style)
        if "index_style" not in wbook.style_names:
            wbook.add_named_style(index_style)
        alpha_insertion = f"alpha{alpha}_"
        if len(wbook.worksheets) > 1:
            x_T =wbook.create_sheet(f"{criterion}_{alpha_insertion if alpha is not None else ''}x_T")
        else:
            x_T = wbook.active
            x_T.title = f"{criterion}_{alpha_insertion if alpha is not None else ''}x_T"
        y_T = wbook.create_sheet(f"{criterion}_{alpha_insertion if alpha is not None else ''}y_T")
        self._fill_sheet(data['x'], x_T)
        self._fill_sheet(data['y'], y_T)
        x_T.cell(len(data['x'])+4, 1).font = Font(bold=True)
        x_T.cell(len(data['x'])+4, 1).value = f"Сумарні витрати на виробництво F(X) = {data['F']}"
        x_T.cell(2, len(data['x'])+2).value = "Тіньова ціна продукта j"
        x_T.cell(2, len(data['x']) + 2).font = Font(bold=True)
        for i, price in enumerate(data["shadow"]):
            x_T.cell(i+3, len(data['x'])+2).value = price
        wbook.save(destination)

    @staticmethod
    def _fill_sheet(data_subdict, wsheet):
        wsheet.column_dimensions['A'].width = len("Продукт j")+1
        wsheet.cell(2, 1).value = "Продукт j"
        wsheet.cell(2, 1).font = Font(bold=True)
        wsheet.merge_cells(start_row=1, start_column=2, end_row=1, end_column=len(data_subdict)+1)
        wsheet.cell(1, 2).value = "Ділянка k"
        wsheet.cell(1, 2).style = "header_style"
        for i, row in enumerate(data_subdict):
            wsheet.cell(i+3, 1).value = i+1
            wsheet.cell(i+3, 1).style = "index_style"
            for j, x in enumerate(row):
                wsheet.cell(2, j+2).value = j+1
                wsheet.cell(2, j+2).style = "index_style"
                wsheet.cell(i+3, j+2).value = x
