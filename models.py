import numpy as np
import numpy.ma as ma
from scipy.optimize import linprog


class Simplex(object):
    def __init__(self, A, b, c_T):
        self.B_size = A.shape[0]
        self.f_size = A.shape[1]
        E = np.eye(self.B_size)
        self.aug_matrix = np.column_stack((A, E, b))
        self.c_T = np.concatenate((c_T, np.zeros(self.B_size)))
        self.c_T_i = np.array([i for i, _ in enumerate(self.c_T)])

    @property
    def matrix(self):
        return self.aug_matrix[:, :-1]

    @property
    def b(self):
        return self.aug_matrix[:, -1]

    @property
    def c_B_i(self):
        return self.c_T_i[self.f_size:]

    @property
    def c_T_B(self):
        return self.c_T[self.c_B_i]

    @property
    def F(self):
        return sum(self.b * self.c_T_B)

    @property
    def deltas(self):
        return np.array([sum(self.matrix[:, j] * self.c_T_B) - c_j for j, c_j in enumerate(self.c_T)])

    def _swap(self, exits, enters):
        self.c_T_i[enters], self.c_T_i[exits + self.f_size] = self.c_T_i[exits + self.f_size], self.c_T_i[enters]

    def _get_min_ratio(self, j):
        pivot_column = ma.masked_where(self.matrix[:, j] < 0, self.matrix[:, j])
        ratio = self.b / pivot_column

        return np.argmin(ratio)

    def _get_pivots(self):
        nonzero_deltas = ma.masked_where(self.deltas == 0, self.deltas)
        pivot_j = np.argmax(nonzero_deltas)
        pivot_i = self._get_min_ratio(pivot_j)

        return pivot_i, pivot_j

    def _gauss_eliminate(self, i, j):
        self._swap(i, j)

        if self.aug_matrix[i, j] != 1:
            self.aug_matrix[i] /= self.aug_matrix[i, j]

        for number, row in enumerate(self.aug_matrix):
            if number != i:
                row -= self.aug_matrix[i] * row[j]

    def _eliminate_negative_b(self):
        while any([b < 0 for b in self.b]):
            pivot_i = np.argmin(self.b)

            if all([j >= 0 for j in self.matrix[pivot_i]]):
                raise Exception("No solution.")

            pivot_row = ma.masked_where(self.matrix[pivot_i] >= 0, self.matrix[pivot_i])
            pivot_j = np.argmin(pivot_row)
            self._gauss_eliminate(pivot_i, pivot_j)

    def optimize(self):  # always minimize
        self._eliminate_negative_b()
        while any(map(lambda delta: delta > 0, self.deltas)):
            i, j = self._get_pivots()
            self._gauss_eliminate(i, j)
        return


class TestSimplex(Simplex):
    def __init__(self, A, b, c_T):
        super().__init__(A, b, c_T)

    def optimize(self):
        return linprog(self.c_T, A_ub=self.matrix, b_ub=self.b)


# if __name__ == '__main__':
