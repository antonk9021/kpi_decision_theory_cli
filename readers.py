import sys
import openpyxl
from abc import ABC, abstractmethod


class AbstractReader(ABC):
    @staticmethod
    @abstractmethod
    def read(source):
        pass


class XlsxReader(AbstractReader):
    @staticmethod
    def read(source):
        data = {"cost": [], "yield": []}
        wb = openpyxl.load_workbook(source, read_only=True)
        a_jk = wb["a_jk"]
        c_jk = wb["c_jk"]
        for sheet in [a_jk, c_jk]:
            assert "ділянка k" in sheet["B1"].value.lower()
            assert "продукт j" in sheet["A2"].value.lower()
        crops_yield = [row for row in a_jk.iter_rows(min_row=3, min_col=2)]
        for row in crops_yield[:-1]:
            cells = list(filter(lambda cell: cell.value is not None, row))
            data["yield"].append({"a_jk": [cell.value for cell in cells[:-1]], "A_j": cells[-1].value})

        data["area"] = [cell.value for cell in crops_yield[-1] if cell.value is not None]

        cost = [row for row in c_jk.iter_rows(min_row=3, min_col=2)]
        for row in cost:
            data["cost"].extend([cell.value for cell in row if cell.value is not None])

        return data


class StreamReader(AbstractReader):
    def read(self, source=sys.stdin):
        data = {"yield": []}
        products = self._input_jk('l', "products")
        crops = self._input_jk('r', "crops")

        print("Please fill in the crops yield table (a_jk and A_j) below:\n")
        for j in range(products):
            a_jk = self._validate_seq(input(f"Enter {crops} a_{j + 1}k`s separated by spaces:\n"), products)
            A_j = input(f"Enter A_{j+1}:\n")
            while True:
                if ' ' in A_j:
                    A_j = input("Please enter a single non-negative real number:\n")
                if not A_j.isnumeric():
                    A_j = input(f"Value `{A_j}` is invalid. Please enter non-negative real numbers only:\n")
                if float(A_j) < 0:
                    A_j = input(f"Value `{A_j}` is invalid. Please enter non-negative real numbers only:\n")
                break
            data["yield"].append({"a_jk": a_jk, "A_j": float(A_j)})
        b_k = self._validate_seq(input(f"Enter crop areas ({crops} b_k`s) separated by spaces:\n"), crops)
        data["area"] = b_k
        c_jk = self._validate_seq(input(f"Enter cost ({products * crops} c_jk`s):\n"), products * crops)
        data["cost"] = c_jk

        return data

    @staticmethod
    def _input_jk(jk_index, jk_string):
        jk_input = input(f"Enter number of {jk_string} ({jk_index}):\n")
        jk_input_error = "Invalid input. Please enter positive integer value:\n"
        while True:
            if not jk_input.isnumeric():
                jk_input = input(jk_input_error)
                continue
            if int(jk_input) <= 0:
                jk_input = input(jk_input_error)
                continue
            return int(jk_input)

    @staticmethod
    def _validate_seq(seq, length):
        splitted = seq.split(' ')
        while True:
            if len(splitted) != length:
                splitted = input(f"Entered sequence has {len(splitted)} value{'s' if len(splitted) > 1 else ''}, while it should have {length}. Please re-enter it:\n").split(' ')
                continue
            if not all([item.isnumeric() for item in splitted]):
                splitted = input(f"Invalid value(s) encountered in {splitted}. Please enter non-negative real numbers only:\n").split(' ')
                continue
            if any([float(i) < 0 for i in splitted]):
                splitted = input(f"Invalid value(s) encountered in {splitted}. Please enter non-negative real numbers only:\n").split(' ')
                continue
            return [float(i) for i in splitted]
