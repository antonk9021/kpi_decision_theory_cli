import numpy as np
from abc import ABC


class AbstractController(ABC):
    def __getitem__(self, item):
        return getattr(self, item)


class InputController(AbstractController):
    def __init__(self, reader, source):
        data = reader.read(source)
        A_ge = []
        for j, a in enumerate(data["yield"]):
            row = []
            for A_j, _ in enumerate(a["A_j"] for a in data["yield"]):
                if j == A_j:
                    row.extend(a["a_jk"])
                else:
                    row.extend([0 for _ in data["area"]])
            A_ge.append(row)
        self.A_ge = -np.array(A_ge)
        A_le = []
        for _ in data["yield"]:
            A_le.append(np.eye(len(data["area"])))
        self.A_le = np.column_stack(A_le)
        b = [-item["A_j"] for item in data["yield"]] + data["area"]
        self.b = np.array(b)
        self.c_T = np.array(data["cost"])
        self.j = len(data["yield"])
        self.k = len(data["area"])


class FuzzyTransformer(AbstractController):
    def __init__(self, A_ge, c_T, alpha):
        self.c_T_opt = c_T - np.sqrt((1 - alpha) / alpha)
        self.A_ge_opt = (1 + np.sqrt((1 - alpha) / 2*alpha)) * A_ge
        self.c_T_pess = c_T + np.sqrt((1 - alpha) / alpha)
        self.A_ge_pess = (1 - np.sqrt((1 - alpha) / 2*alpha)) * A_ge


class MatrixMaker(AbstractController):
    def __init__(self, A_le, A_ge, b, c_T):
        self.A = np.vstack((A_ge, A_le))
        self.b = b
        self.c_T = c_T


class OutputController(AbstractController):
    def __init__(self, k, F, b, c_length, c_B_i, deltas):
        self.F = F
        b_dict = dict(zip(c_B_i, b))
        x = [b_dict[i] if i in c_B_i else 0 for i in range(c_length)]
        self.x = np.array([x[j: j+k] for j in range(0, len(x), k)])
        y = abs(deltas[:c_length]) #if all(deltas > 0) else -deltas[:c_length]
        self.y = np.array([y[j: j+k] for j in range(0, len(y), k)])
        shadow = deltas[c_length:] if all(deltas > 0) else -deltas[c_length:]
        self.shadow = shadow[:len(self)]

    def __len__(self):
        return self.x.shape[0]
