import argparse
from models import *
import sys
from controllers import *
from readers import *
from writers import *


def main(tab_source=sys.stdin, output=sys.stdout, fuzzy=False, alpha=None, test=False):
    if tab_source in [sys.stdin, None]:
        reader = StreamReader()
    elif ".xls" in tab_source:
        print(f"Using input from {tab_source}")
        reader = XlsxReader()
    else:
        raise Exception("Wrong input file extension. Please use .xlsx or .xls")

    if output in [sys.stdout, None]:
        writer = StreamWriter()
    elif ".xls" in output:
        writer = XlsxWriter()
    else:
        raise Exception("Wrong output file extension. Please use .xlsx or .xls")

    ic = InputController(reader, tab_source)
    to_solve = {}

    if fuzzy:
        if alpha is None:
            raise Exception("Alpha not specified.")

        elif alpha <= 0 or alpha >= 1:
            raise Exception("Alpha should lie between 0 and 1 exclusively.")
        else:
            ft = FuzzyTransformer(ic.A_ge, ic.c_T, alpha)
            for criterion in ["opt", "pess"]:
                to_solve[criterion] = {"c_T": ft[f"c_T_{criterion}"],
                                 "A_ge": ft[f"A_ge_{criterion}"],
                                 "A_le": ic["A_le"],
                                 "b": ic["b"]}

    else:
        to_solve["lp"] = {"c_T": ic.c_T, "A_ge": ic.A_ge, "A_le": ic["A_le"], "b": ic["b"]}

    for criterion in to_solve:
        mm = MatrixMaker(**to_solve[criterion])
        if test:
            s = TestSimplex(A=mm.A, b=mm.b, c_T=mm.c_T)
            print(f"Criterion: {criterion}\n{s.optimize()}\n")
            continue
        else:
            s = Simplex(A=mm.A, b=mm.b, c_T=mm.c_T)

            s.optimize()
            oc = OutputController(k=ic.k, F=s.F, b=s.b, c_length=s.f_size, c_B_i=s.c_B_i, deltas=s.deltas)
            writer.write(oc, criterion, alpha, output)
    if output not in [sys.stdout, None]:
        print(f"Results written to {output}")

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", dest="input")
    parser.add_argument("-o", "--output", dest="output")
    parser.add_argument("-f", "--fuzzy", dest="fuzzy", action="store_true")
    parser.add_argument("-a", "--alpha", dest="alpha", type=float)
    parser.add_argument("-t", "--test", dest="test", action="store_true")
    args = parser.parse_args(sys.argv[1:])
    main(args.input, args.output, args.fuzzy, args.alpha, args.test)
